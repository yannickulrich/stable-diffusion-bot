try:
    from .sd import run as run_stable_diffusion
except ImportError:
    from .sd_mock import run as run_stable_diffusion

import subprocess
from .config import Config
import io
import asyncio
import aiofiles
import os
import mistune
from nio import (
    AsyncClient,
    MatrixRoom,
    RoomMessageText,
    LoginResponse,
    UploadResponse,
    ProfileSetDisplayNameResponse,
    ProfileGetAvatarError,
    ProfileSetAvatarError,
    RoomCreateError
)
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Bot:
    def __init__(self):
        self.ready = False

    @property
    def version(self):
        v, _ = subprocess.Popen(
            ['git', 'rev-parse', '--short', 'HEAD'],
            stdout=subprocess.PIPE
        ).communicate()
        return v.strip().decode()

    async def message_callback(self, room, event):
        logger.debug(
            f"Received message in room {room.display_name} "
            f"from {room.user_name(event.sender)}"
        )

        if room.user_name(event.sender) == self.config.device_name:
            logger.debug("Message is from us, skip..")
            return
        if hasattr(self, 'name'):
            if room.user_name(event.sender) == self.name:
                logger.debug("Message is from us, skip..")
                return

        logger.info(f"Message received in room {room.display_name}")
        logger.info(f"{room.user_name(event.sender)} | {event.body}")

        if event.body.strip().startswith("!"):
            args = event.body.strip()[1:].split(" ")
            cmd = args[0]
            logger.info(f"Command is {cmd}, arguments: {' '.join(args[1:])}")
            if hasattr(self, cmd):
                f = getattr(self, cmd)
                await self.send(room.room_id, await f(room.room_id, *args[1:]))
                logger.info(f"Handled command {cmd}")
            else:
                logger.warn(f"Can't understand command {cmd}")
                await self.send(
                    room.room_id,
                    f"I don't understand `{cmd}`. "
                    f"Run `!help` for available commands"
                )
        else:
            await self.handle(room.room_id, event.body.strip())

    async def help(self, rid):
        return self.help_str

    async def set_profile(self):
        if hasattr(self, 'name'):
            logger.info(f"display_name -> {self.name}")
            resp = await self.client.set_displayname(self.name)
            if type(resp) != ProfileSetDisplayNameResponse:
                logger.error(f"Couldn't set display name:")
                logger.error(resp)

        if hasattr(self, 'avatar'):
            logger.info(f"set_avatar -> {self.avatar}")
            current_avatar = await self.client.get_avatar()
            if type(current_avatar) != ProfileGetAvatarError:
                logger.warn("Avatar already set, won't set again")
                logger.debug(f"Avatar is {current_avatar.avatar_url}")
                return

            # Upload the image to the homeserver
            avatar_stat = os.stat(self.avatar)
            async with aiofiles.open(self.avatar, "rb") as avatar:
                logger.info(
                    f"Uploading {self.avatar} ({avatar_stat.st_size} bytes)"
                )
                upload_response, _ = await self.client.upload(
                    avatar,
                    content_type="image/png",
                    filename=self.avatar,
                    filesize=avatar_stat.st_size
                )

            # Set the uploaded image as our avatar
            if type(upload_response) == UploadResponse:
                resp = await self.client.set_avatar(
                    upload_response.content_uri
                )
                if type(resp) != ProfileSetAvatarError:
                    logger.error("Setting avatar failed")
                    logger.error(resp)
            else:
                logger.error("Upload of avatar failed")
                logger.error(upload_response)

    async def login(self):
        if self.config.access_token == "":
            logger.info("Starting login procedure using password")
            self.client = AsyncClient(
                self.config.homeserver, self.config.username
            )
            resp = await self.client.login(
                self.config.password, device_name=self.config.device_name
            )
            if not isinstance(resp, LoginResponse):
                logger.error("Login using password failed")
                logger.error(resp)
            self.config.access_token = resp.access_token
            self.config.device_id = resp.device_id
        else:
            logger.info("Starting login procedure using token")
            self.client = AsyncClient(self.config.homeserver)
            self.client.access_token = self.config.access_token
            self.client.user_id = self.config.username
            self.client.device_id = self.config.device_id

        logger.info("Synchronising")
        await self.client.sync()
        await self.set_profile()
        logger.info("Setting online")
        await self.client.set_presence('online')
        self.client.add_event_callback(
            self.message_callback, RoomMessageText
        )

        for room in self.config.rooms:
            if 'room' not in room:
                logger.warn("No room for user {room['name']}, creating new")
                room['room'] = await self.create_room(room['name'])
                logger.warn(f"room for {room['name']} -> {room['room']}")

            await self.send(
                room['room'],
                f"SD bot `v{self.version}`\n\n"
                f" Type `!help` for available commands"
            )
        logger.info("Ready")
        self.ready = True

    async def close(self):
        if self.ready:
            logger.info("Shutting down")
            await self.client.set_presence('offline')
            await self.client.close()

    async def create_room(self, user):
        resp = await self.client.room_create(
            federate=False,
            is_direct=True,
            invite=[user]
        )
        if isinstance(resp, RoomCreateError):
            logger.error("Room creation failed")
            logger.error(resp)
            raise ValueError
        else:
            return resp.room_id

    async def delete_room(self, room):
        await self.client.room_kick(room['room'], room['name'])
        await self.client.room_leave(room['room'])

    async def send(self, room, msg):
        logger.info(f"Sending message to room {room}: {msg}")
        return await self.client.room_send(
            room_id=room,
            message_type="m.room.message",
            content={
                "msgtype": "m.text",
                "format": "org.matrix.custom.html",
                "formatted_body": mistune.markdown(msg),
                "body": msg
            }
        )

    async def send_image(self, room, fn, image):
        fp = io.BytesIO()
        image.save(fp, format='PNG')
        size = fp.tell()
        fp.seek(0)

        resp, maybe_keys = await self.client.upload(
            fp,
            content_type='image/png',
            filename=fn,
            filesize=size,
        )

        if isinstance(resp, UploadResponse):
            pass
        else:
            return f"Failed to upload image. Failure response: {resp}"

        await self.client.room_send(
            room, message_type="m.room.message", content={
                "body": fn,
                "info": {
                    "size": size,
                    "mimetype": "image/png",
                    "thumbnail_info": None,
                    "thumbnail_url": None,
                    "w": image.width, "h": image.height
                },
                "msgtype": "m.image",
                "url": resp.content_uri
            }
        )


class SDBot(Bot):
    help_str = (
        "Send a message as the prompt. Commands are to be"
        " prefixed with `!`. Available commands are `!set "
        "<parameter> <value>` and `!options [parameters..]`."
    )
    avatar = 'icon.png'
    name = "Drawing Bot"

    def __init__(self, loop, config):
        self.config = config
        self.loop = loop

    def get_sd_config(self, rid):
        for r in self.config.rooms:
            if r["room"] == rid:
                return r["stable_diffusion_config"]

    async def run(self):
        logger.info("Starting main loop")
        await self.client.sync_forever(timeout=30000)

    async def options(self, rid, *args):
        sdc = self.get_sd_config(rid)
        if len(args) == 0:
            args = sdc._schema.keys()

        ans = "Current configuration:\n"
        for arg in args:
            if hasattr(sdc, arg):
                ans += f" * `{arg}`: `{sdc.__getattribute__(arg)}`\n"
            else:
                ans += f" * `{arg}` not found"

        return ans

    async def set(self, rid, opt, val):
        sdc = self.get_sd_config(rid)
        try:
            sdc.__setattr__(opt, val)
        except KeyError:
            return f"Error: {opt} does not exist"
        except AttributeError:
            t = sdc._schema[opt].rule_description
            return f"Error: {opt} needs to be {t}."

    async def handle(self, rid, msg):
        await self.client.room_typing(rid, typing_state=True, timeout=10000)
        sdc = self.get_sd_config(rid)
        gen = run_stable_diffusion(
            msg,
            self.config.stable_diffusion_root,
            self.get_sd_config(rid)
        )
        for n, im in enumerate(gen):
            await self.send_image(rid, f"image{n}.png", im)
        await self.client.room_typing(rid, typing_state=False)


if __name__ == "__main__":
    conf = Config('config.yaml')
    loop = asyncio.new_event_loop()
    bot = SDBot(loop, conf)

    loop.run_until_complete(
        bot.login()
    )
    try:
        loop.create_task(bot.run())
        loop.run_forever()
    except Exception:
        loop.run_until_complete(bot.close())
