from PIL import Image
import random
import os
import time


def run(prompt, root, opt={}):
    print(prompt, opt)
    folder = '/tmp/txt2img-samples/samples/'
    time.sleep(5)
    for i in random.choices(os.listdir(folder), k=2):
        time.sleep(2)
        yield Image.open(os.path.join(folder, i))
