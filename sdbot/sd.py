import os
import torch
import numpy as np
from omegaconf import OmegaConf
from PIL import Image
from tqdm import tqdm, trange
from einops import rearrange
from pytorch_lightning import seed_everything
from torch import autocast
from contextlib import nullcontext

from ldm.util import instantiate_from_config
from ldm.models.diffusion.ddim import DDIMSampler
from ldm.models.diffusion.plms import PLMSSampler
from ldm.models.diffusion.dpm_solver import DPMSolverSampler

torch.set_grad_enabled(False)


def load_model_from_config(config, ckpt, verbose=False):
    print(f"Loading model from {ckpt}")
    pl_sd = torch.load(ckpt, map_location="cpu")
    if "global_step" in pl_sd:
        print(f"Global Step: {pl_sd['global_step']}")
    sd = pl_sd["state_dict"]
    model = instantiate_from_config(config.model)
    m, u = model.load_state_dict(sd, strict=False)
    if len(m) > 0 and verbose:
        print("missing keys:")
        print(m)
    if len(u) > 0 and verbose:
        print("unexpected keys:")
        print(u)

    model.cuda()
    model.eval()
    return model


class Conf:
    steps = 50
    plms = False
    dpm = False
    fixed_code = False
    ddim_eta = 0.
    n_iter = 3
    H = 512
    W = 512
    C = 4
    f = 8
    n_samples = 3
    scale = 9
    config = '../stablediffusion/configs/stable-diffusion/v2-inference-v.yaml'
    ckpt = '../stable-diffusion-2/768-v-ema.ckpt'
    seed = 42
    precision = 'autocast'  # or full
    repeat = 1


def run(prompt, root, opt=Conf()):
    seed_everything(opt.seed)

    config = OmegaConf.load(os.path.join(root, f"{opt.config}"))
    model = load_model_from_config(config, os.path.join(root, f"{opt.ckpt}"))

    if torch.cuda.is_available():
        device = torch.device("cuda")
    else:
        device = torch.device("cpu")

    model = model.to(device)

    if opt.plms:
        sampler = PLMSSampler(model)
    elif opt.dpm:
        sampler = DPMSolverSampler(model)
    else:
        sampler = DDIMSampler(model)

    batch_size = opt.n_samples
    data = [batch_size * [prompt]]

    start_code = None
    if opt.fixed_code:
        start_code = torch.randn([
            opt.n_samples, opt.C, opt.H // opt.f, opt.W // opt.f
        ], device=device)

    precision_scope = autocast if opt.precision == "autocast" else nullcontext
    with torch.no_grad(), precision_scope("cuda"), model.ema_scope():
        for n in trange(opt.n_iter, desc="Sampling"):
            for prompts in tqdm(data, desc="data"):
                uc = None
                if opt.scale != 1.0:
                    uc = model.get_learned_conditioning(batch_size * [""])
                if isinstance(prompts, tuple):
                    prompts = list(prompts)
                c = model.get_learned_conditioning(prompts)
                shape = [opt.C, opt.H // opt.f, opt.W // opt.f]
                samples, _ = sampler.sample(
                    S=opt.steps,
                    conditioning=c,
                    batch_size=opt.n_samples,
                    shape=shape,
                    verbose=False,
                    unconditional_guidance_scale=opt.scale,
                    unconditional_conditioning=uc,
                    eta=opt.ddim_eta,
                    x_T=start_code
                )

                x_samples = model.decode_first_stage(samples)
                x_samples = torch.clamp(
                    (x_samples + 1.0) / 2.0,
                    min=0.0, max=1.0
                )

                for x_sample in x_samples:
                    x_sample = 255. * rearrange(
                        x_sample.cpu().numpy(), 'c h w -> h w c'
                    )
                    img = Image.fromarray(x_sample.astype(np.uint8))
                    yield img
