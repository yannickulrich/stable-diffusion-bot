from strictyaml import *
import types

_sd_schema = {
    Optional("default", False): Bool(),
    Optional("steps", 50): Int(),
    Optional("plms", False): Bool(),
    Optional("dpm", False): Bool(),
    Optional("fixed_code", False): Bool(),
    Optional("ddim_eta", 0.): Float(),
    Optional("n_iter", 3): Int(),
    Optional("H", 512): Int(),
    Optional("W", 512): Int(),
    Optional("C", 4): Int(),
    Optional("f", 8): Int(),
    Optional("n_samples", 3): Int(),
    Optional("scale", 9.): Float(),
    Optional(
        "config",
        "stablediffusion/configs/stable-diffusion/v2-inference-v.yaml"
    ): Str(),
    Optional("ckpt", "stable-diffusion-2/768-v-ema.ckpt"): Str(),
    Optional("seed", 42): Int(),
    Optional("precision", "autocast"): Enum(["autocast", "full"]),
    Optional("repeat", 1): Int()
}

_schema = Map({
    "config": Map({
        "homeserver": Url(),
        "username": Str(),
        "password": Str(),
        Optional("access_token", ""): Str(),
        Optional("device_id", ""): Str(),
        Optional("device_name", "diffuser"): Str(),
        "stable_diffusion_root": Str(),
        "rooms": Seq(
            Map({
                "name": Str(),
                Optional("room"): Str(),

                "stable_diffusion_config": Map(_sd_schema)
            })
        )
    })
})


class Chunk:
    def __init__(self, c):
        self.contents = c


class StableDiffusionConfig(types.SimpleNamespace):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._schema = {i.key: j for i, j in _sd_schema.items()}

    def __setattr__(self, key, value):
        if isinstance(value, str) and not key.startswith("_"):
            super().__setattr__(
                key,
                self._schema[key].validate(Chunk(value))
            )
        else:
            super().__setattr__(key, value)


class Config(types.SimpleNamespace):
    def __init__(self, fn):
        with open(fn) as fp:
            super().__init__(**load(fp.read(), _schema).data['config'])
            self.rooms = [
                {
                    "name": i["name"],
                    "room": i["room"],
                    "stable_diffusion_config": StableDiffusionConfig(
                        **i['stable_diffusion_config']
                    )
                }
                for i in self.rooms
            ]
